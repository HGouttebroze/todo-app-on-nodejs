const mongoose = require("mongoose");
mongoose.connect(
  "mongodb://localhost:27017/node-api",
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (!err) console.log("mongoodb is connected");
    else console.log("error connection");
  }
);

// const Cat = mongoose.model("Cat", { name: String });

// const kitty = new Cat({ name: "Zildjian" });
// kitty.save().then(() => console.log("meow"));

// var Customer = mongoose.model("Customer", {
//   first_name: String,
//   last_name: String,
//   gender: String,
//   age: Number,
//   email: String,
// });

// module.exports = { Customer };
